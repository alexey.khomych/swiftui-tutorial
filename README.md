# SwiftUI Tutorial

## Description

There is a simple SwiftUI project where I learned the basics of working with SwiftUI, async/await, speech recognizer. The project allows you to manage your scrum meetings. You could add a new one, edit old one and convert speech to text, then save it in history. 
For the future, I plan change saving data in .data files to CoreData and use Combine for different things. 

Tutorial by Apple: https://developer.apple.com/tutorials/app-dev-training/getting-started-with-scrumdinger 

## Project status

On going...
