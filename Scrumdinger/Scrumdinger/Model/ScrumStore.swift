//
//  ScrumStore.swift
//  Scrumdinger
//
//  Created by Alexey Khomych on 19.02.2023.
//

import Foundation
import SwiftUI

///ObservableObject is a class-constrained protocol for connecting external model data to SwiftUI views
class ScrumStore: ObservableObject {
    
    // MARK: - Public Variables
    
    /*
     An ObservableObject includes an objectWillChange publisher that emits when one of its @Published
     properties is about to change. Any view observing an instance of ScrumStore will render again
     when the scrums value changes.
     */
    @Published var scrums: [DailyScrum] = []
    
    // MARK: - Private
    
    private static func fileURL() throws -> URL {
        try FileManager.default.url(
            for: .documentDirectory,
            in: .userDomainMask,
            appropriateFor: nil,
            create: false
        )
        .appendingPathComponent("scrums.data")
    }
    
    // MARK: - Public
    
    static func load() async throws -> [DailyScrum] {
        try await withCheckedThrowingContinuation { continuation in
            load { result in
                switch result {
                case let .failure(error):
                    continuation.resume(throwing: error)
                case let .success(scrums):
                    continuation.resume(returning: scrums)
                }
            }
        }
    }
    
    static func load(completion: @escaping (Result<[DailyScrum], Error>) -> Void) {
        DispatchQueue.global(qos: .background).async {
            do {
                let fileURL = try fileURL()
                guard let file = try? FileHandle(forReadingFrom: fileURL) else {
                    DispatchQueue.main.async {
                        completion(.success([]))
                    }
                    return
                }
                
                let dailyScrums = try JSONDecoder().decode([DailyScrum].self, from: file.availableData)
                DispatchQueue.main.async {
                    completion(.success(dailyScrums))
                }
            } catch {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
        }
    }
    
    static func save(scrums: [DailyScrum], completion: @escaping (Result<Int, Error>) -> Void) {
        DispatchQueue.global(qos: .background).async {
            do {
                let data = try JSONEncoder().encode(scrums)
                let outfile = try fileURL()
                try data.write(to: outfile)
                DispatchQueue.main.async {
                    completion(.success(scrums.count))
                }
            } catch {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
        }
    }
    
    @discardableResult
    static func save(scrums: [DailyScrum]) async throws -> Int {
        try await withCheckedThrowingContinuation { continuation in
            save(scrums: scrums, completion: { result in
                switch result {
                case let .failure(error):
                    continuation.resume(throwing: error)
                case let .success(scrumsSaved):
                    continuation.resume(returning: scrumsSaved)
                }
            })
        }
    }
}
