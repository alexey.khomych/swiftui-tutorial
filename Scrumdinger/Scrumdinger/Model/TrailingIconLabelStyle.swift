//
//  TrailingIconLabelStyle.swift
//  Scrumdinger
//
//  Created by Alexey Khomych on 06.02.2023.
//

import SwiftUI

struct TrailingIconLabelStyle: LabelStyle {

    // MARK: - Public Variables
    
    func makeBody(configuration: Configuration) -> some View {
        HStack {
            configuration.title
            configuration.icon
        }
    }
}

// MARK: - TrailingIconLabelStyle

extension LabelStyle where Self == TrailingIconLabelStyle {
    
    static var trailingIcon: Self {
        Self()
    }
}
