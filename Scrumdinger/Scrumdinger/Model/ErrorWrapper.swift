//
//  ErrorWrapper.swift
//  Scrumdinger
//
//  Created by Alexey Khomych on 21.02.2023.
//

import Foundation

struct ErrorWrapper: Identifiable {
    
    // MARK: - Public Variables
    
    let id: UUID
    let error: Error
    let guidance: String
    
    // MARK: - Initializer
    
    init(id: UUID = UUID(), error: Error, guidance: String) {
        self.id = id
        self.error = error
        self.guidance = guidance
    }
}
