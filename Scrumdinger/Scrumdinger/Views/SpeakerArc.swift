//
//  SpeakerArc.swift
//  Scrumdinger
//
//  Created by Alexey Khomych on 22.02.2023.
//

import SwiftUI

struct SpeakerArc: Shape {

    // MARK: - Public Variables
    
    let speakerIndex: Int
    let totalSpeakers: Int
    
    // MARK: - Private Variables
    
    private var degreesPerSpeaker: Double {
        360.0 / Double(totalSpeakers)
    }
    
    private var startAngle: Angle {
        /// The additional 1.0 degree is for visual separation between arc segments
        Angle(degrees: degreesPerSpeaker * Double(speakerIndex) + 1.0)
    }

    private var endAngle: Angle {
        /// The subtracted 1.0 degree is for visual separation between arc segments
        Angle(degrees: startAngle.degrees + degreesPerSpeaker - 1.0)
    }
    
    // MARK: - Public
    
    func path(in rect: CGRect) -> Path {
        let diameter = min(rect.size.width, rect.size.height) - 24.0
        let radius = diameter / 2
        let center = CGPoint(x: rect.midX, y: rect.midY)
        
        return Path { path in
            path.addArc(
                center: center,
                radius: radius,
                startAngle: startAngle,
                endAngle: endAngle,
                clockwise: false
            )
        }
    }
}
