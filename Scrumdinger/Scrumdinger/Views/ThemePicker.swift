//
//  ThemePicker.swift
//  Scrumdinger
//
//  Created by Alexey Khomych on 15.02.2023.
//

import SwiftUI

struct ThemePicker: View {
    
    // MARK: - Public Variables
    
    @Binding var selection: Theme
    
    // MARK: - Public
    
    var body: some View {
        Picker("Theme", selection: $selection) {
            ForEach(Theme.allCases) { theme in
                ThemeView(theme: theme)
                    .tag(theme)
            }
        }
    }
}

// MARK: - PreviewProvider

struct ThemePicker_Previews: PreviewProvider {
    
    static var previews: some View {
        /// default value for binding property
        ThemePicker(selection: .constant(.periwinkle))
    }
}
