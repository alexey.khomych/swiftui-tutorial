//
//  ThemeView.swift
//  Scrumdinger
//
//  Created by Alexey Khomych on 15.02.2023.
//

import SwiftUI

struct ThemeView: View {
    
    // MARK: - Public Variables
    
    let theme: Theme
    
    // MARK: - Public
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 4)
                .fill(theme.mainColor)
            Label(theme.name, systemImage: "paintpalette")
                .padding(4)
        }
        .foregroundColor(theme.accentColor)
        .fixedSize(horizontal: false, vertical: true)
    }
}

// MARK: - PreviewProvider

struct ThemeView_Previews: PreviewProvider {
    
    static var previews: some View {
        ThemeView(theme: .buttercup)
    }
}
