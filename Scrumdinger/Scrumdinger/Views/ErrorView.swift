//
//  ErrorView.swift
//  Scrumdinger
//
//  Created by Alexey Khomych on 21.02.2023.
//

import SwiftUI

struct ErrorView: View {
    
    // MARK: - Public Variables
    
    let errorWrapper: ErrorWrapper
    
    // MARK: - Private Variables
    
    ///access the view’s dismiss structure and call it like a function to dismiss the view.
    @Environment(\.dismiss) private var dismiss
    
    // MARK: - Public
    
    var body: some View {
        NavigationView {
            VStack {
                Text("An error has occured!")
                    .font(.title)
                    .padding(.bottom)
                Spacer()
                Text(errorWrapper.error.localizedDescription)
                    .font(.headline)
                Text(errorWrapper.guidance)
                    .font(.caption)
                    .padding(.top)
            }
            .padding()
            .background(.ultraThinMaterial)
            .cornerRadius(16)
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button("Dismiss") {
                        dismiss()
                    }
                }
            }
        }
    }
}

// MARK: - PreviewProvider

struct ErrorView_Previews: PreviewProvider {
    
    enum SampleError: Error {
        case errorRequired
    }
    
    static var wrapper: ErrorWrapper {
        ErrorWrapper(
            error: SampleError.errorRequired,
            guidance: "You can safely ignore this error."
        )
    }
    
    static var previews: some View {
        ErrorView(errorWrapper: wrapper)
    }
}
