//
//  ScrumsView.swift
//  Scrumdinger
//
//  Created by Alexey Khomych on 10.02.2023.
//

import SwiftUI

struct ScrumsView: View {
    
    // MARK: - Public Variables
    
    @Binding var scrums: [DailyScrum]
    let saveAction: () -> Void
    
    // MARK: - Private Variables
    
    ///observe value and save user data when it becomes inactive.
    @Environment(\.scenePhase) private var scenePhase

    @State private var isPresentingNewScrumView = false
    @State private var newScrumData = DailyScrum.Data()
    
    // MARK: - Public
    
    var body: some View {
        List {
            ///The $ prefix accesses the projected value of a wrapped property. The projected value of the scrums binding is another binding.
            ForEach($scrums) { $scrum in
                NavigationLink(destination: DetailView(scrum: $scrum)) {
                    CardView(scrum: scrum)
                }
                .listRowBackground(scrum.theme.mainColor)
            }
        }
        .navigationTitle("Daily Scrums")
        .toolbar {
            Button(action: {
                isPresentingNewScrumView = true
            }) {
                Image(systemName: "plus")
            }
            .accessibilityLabel("New Scrum")
        }
        .sheet(isPresented: $isPresentingNewScrumView) {
            NavigationView {
                DetailEditView(data: $newScrumData)
                    .toolbar {
                        ToolbarItem(placement: .cancellationAction) {
                            Button("Dismiss") {
                                isPresentingNewScrumView = false
                                newScrumData = DailyScrum.Data()
                            }
                        }
                        ToolbarItem(placement: .confirmationAction) {
                            Button("Add") {
                                let newScrum = DailyScrum(data: newScrumData)
                                scrums.append(newScrum)
                                isPresentingNewScrumView = false
                                newScrumData = DailyScrum.Data()
                            }
                        }
                    }
            }
        }
        .onChange(of: scenePhase) { phase in
            if phase == .inactive {
                saveAction()
            }
        }
    }
}

// MARK: - PreviewProvider

struct ScrumsView_Previews: PreviewProvider {
    
    static var previews: some View {
        NavigationView {
            ScrumsView(scrums: .constant(DailyScrum.sampleData), saveAction: {})
        }
    }
}
